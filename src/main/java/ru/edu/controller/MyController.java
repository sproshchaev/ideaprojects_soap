package ru.edu.controller;

import org.springframework.web.bind.annotation.*;
import ru.edu.dto.CityInfoResponse;
import ru.edu.dto.CityInformation2;
import ru.edu.dto.HelloResponse;

import java.time.LocalDateTime;

// 37:04 - создание пакета controller
@RestController
@RequestMapping("/example")
public class MyController {

    // запрос http://localhost:8080/example/hello
    @GetMapping("/hello")
    public HelloResponse helloWorld() {

        HelloResponse response = new HelloResponse();
        response.setMessage("Hello World!");
        return response;

    }

    // http://localhost:8080/example/city?id=msk
    @GetMapping("/city")
    public CityInfoResponse getCityInfo(@RequestParam("id") String cityId) {

        CityInfoResponse response = new CityInfoResponse();
        response.setCityId(cityId);
        response.setName("Moscow");
        response.setResponseTime(LocalDateTime.now().toString());

        return response;

    }

    // http://localhost:8080/example/cityi/msk
    @GetMapping("/cityi/{id}")
    public CityInfoResponse getCityInfoI(@PathVariable("id") String cityId) {

        CityInfoResponse response = new CityInfoResponse();
        response.setCityId(cityId);
        response.setName("Moscow");
        response.setResponseTime(LocalDateTime.now().toString());

        return response;

    }

    // actions
    // http://localhost:8080/example/cityii/msk
    @GetMapping("/cityii/{id}")
    public CityInformation2 getCityInfoII(@PathVariable("id") String cityId) {

        CityInformation2 response = new CityInformation2();
        response.setId(cityId);
        response.setName("Moscow");
        response.setResponseTime(LocalDateTime.now().toString());

        // actions
        response.setDetailedInfoHref("http://localhost:8080/api/city/detailed?id=" + cityId);
        response.setDeleteHref("http://localhost:8080/api/city/delete?id=" + cityId);

        return response;

    }

    // actions 2
    // http://localhost:8080/example/cityiii/msk
    @GetMapping("/cityiii/{id}")
    public CityInformation2 getCityInfoIII(@PathVariable("id") String cityId) {

        CityInformation2 response = new CityInformation2();
        response.setId(cityId);
        response.setName("Moscow");
        response.setResponseTime(LocalDateTime.now().toString());

        // actions
        response.setDetailedInfoHref("http://localhost:8080/api/city/" + cityId + "/detailed?id=" + cityId);
        response.setDeleteHref("http://localhost:8080/api/city/delete?id=" + cityId);

        return response;

    }

}
